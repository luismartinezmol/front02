import { LitElement, html } from 'lit-element';  
class PersonaForm extends LitElement {
	static get properties() {
		return {			
		};
	}

	constructor() {
		super();				
	}

	render() {
		return html`	
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">			
			<div>
				<form>
					<div class="form-group">
						<label>Nombre Completo</label>
						<input type="text" id="personFormName" class="form-control" placeholder="Nombre Completo"/>
					<div>
					<div class="form-group">
						<label>Perfil</label>
						<textarea class="form-control" placeholder="Perfil" rows="5"></textarea>
					<div>
					<div class="form-group">
						<label>Años en la empresa</label>
						<input type="text" class="form-control" placeholder="Años en la empresa"/>
					<div>
					<button class="btn btn-primary"><strong>Atrás</strong></button>
					<button class="btn btn-success"><strong>Guardar</strong></button>
				</form>
			</div>
		`;
	}    
}  
customElements.define('persona-form', PersonaForm)    